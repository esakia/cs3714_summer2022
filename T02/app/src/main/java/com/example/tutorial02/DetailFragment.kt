package com.example.tutorial02



import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import com.example.tutorial02.databinding.FragmentDetailBinding


/**
 * A simple [Fragment] subclass.
 *
 */
class DetailFragment : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!
    //current dept
    var dept:String? = null
    var news:String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        bundle: Bundle?
    ): View? {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        val v = binding.root


        return v

    }

    override fun onViewCreated(view: View, bundle: Bundle?) {
        super.onViewCreated(view, bundle)
        dept = this.arguments?.getString("dept")
        news =  this.arguments?.getString("news")


       binding.dept.text = dept
        binding.news.text = news


        val resId = resources.getIdentifier(dept+"url", "string", context?.packageName)
        //opens next fragment with webview
        binding.news.setOnClickListener { v ->
            if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT)
                v?.findNavController()?.navigate(
                    R.id.action_detailFragment_to_wikiFragment,
                    bundleOf("url" to resources.getString(resId))
                )
        }
    }

    fun displayNews(news: NewsItem){
        binding.dept.text = news.dept
        binding.news.text = news.content
    }

}
