package com.example.minichallenge2


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.minichallenge2.databinding.FragmentConfirmBinding


class ConfirmationFragment : Fragment() {


    private var _binding: FragmentConfirmBinding? = null
    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
                             ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentConfirmBinding.inflate(inflater, container, false)
        val v = binding.root



        val model: MyViewModel by activityViewModels()


        // "%.2f".format(..) lets you round to 2 decimal places.
        // ?: is the 'Elvis' operator -- read here:https://kotlinlang.org/docs/reference/null-safety.html#elvis-operator
        val cost = "%.4f".format(arguments?.getFloat("cost") ?: 0f).toFloat()
        binding.message.text =
            "${resources.getString(R.string.confirm_message)}$cost${resources.getString(R.string.confirm_proceed)}"

        binding.purchase.setOnClickListener{


            if(model.cashInWallet>cost){
                model.addOrder(cost)
                v.findNavController().navigate(R.id.action_confirmFragment_to_receiptFragment)
               }
            else{
                Toast.makeText(this.context, resources.getString(R.string.low_funds), Toast.LENGTH_SHORT).show()
              }
        }

        return v
    }


}
