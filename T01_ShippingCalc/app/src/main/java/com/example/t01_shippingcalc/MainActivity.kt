package com.example.t01_shippingcalc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import android.widget.TextView
import com.example.t01_shippingcalc.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    var usdSeekBar: SeekBar? = null
    var lbsSeekBar: SeekBar?= null
    var usdTotal: TextView?= null
    var lbsTotal: TextView?= null

    //this is very important as it helps us 'connect' to the views in the XML
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)

//        usdSeekBar = findViewById(R.id.usd_seek_bar)
//        lbsSeekBar = findViewById(R.id.lbs_seek_bar)
//        usdTotal =   findViewById(R.id.usd_total)
//        lbsTotal =   findViewById(R.id.lbs_total)
        usdSeekBar = binding.usdSeekBar
        lbsSeekBar = binding.lbsSeekBar
        usdTotal = binding.usdTotal
        lbsTotal = binding.lbsTotal

        setBoundaries()

        usdSeekBar?.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{

            var usd_sync=0

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                //progress changed
                usdTotal?.text = "USD:$progress"
                usd_sync=progress

              //  findViewById<TextView>(R.id.total).text = "Total cost:"+progress

                //we can also avoid declaring a local variable for a view and instead locate it via 'binding'
                binding.total.text = "Total cost:"+progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                //when the change starts
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                if(usd_sync.toFloat()<10*1.75)
                    lbsSeekBar?.progress = (usd_sync / 1.75f).toInt()
                else
                    lbsSeekBar?.progress = (usd_sync / 1.25f).toInt()
            }
        })

        lbsSeekBar?.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{

            var lbs_sync=0

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                //progress changed

                lbsTotal?.text = "lbs:$progress"
                lbs_sync=progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                if(lbs_sync<10)
                    usdSeekBar?.progress = (lbs_sync*1.75f).toInt()
                else
                    usdSeekBar?.progress = (lbs_sync*1.25f).toInt()
            }
        })

    }


    private fun setBoundaries(){

        usdSeekBar?.min = 1

        usdSeekBar?.max = 125

        lbsSeekBar?.min = 1

        lbsSeekBar?.max = 100

    }



}